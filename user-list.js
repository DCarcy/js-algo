class User {
    constructor(){
        this.name = '' ;
        this.firstName = '' ;
        this.id = '' ;
        this.password = '' ;
    }
}
/**
 * unique (user: User, list: User[])
 * @param user: User Une variable de type User
 * @param list: User[] un tableau de User
 * @returns boolean
 */

function unique(user, list) {
    let yesYouCan = true ;
    for (let index = 0 ; index < list.length ; index = index + 1 ){
        if (list[index].id === user.id ) {
            yesYouCan = false ;
        }
    }
    return yesYouCan ;
}

/**
 * 
 * @param {User} user Some user variable
 * @param {User[]} list A list of users
 * @return User[] 
 */

function ajouter(user, list) {
   if (unique(user, list)) {
       list.push(user);
   }
   return list;
}

/**
 * Debut application Administrateur
 */

let listUsers = [] ;

const user = new User() ;
user.name = 'Aubert' ;
user.firstName = 'JL' ;
user.id = 'jlaubert' ;
user.password = 'admin' ;
listUsers = ajouter(user, listUsers) ;

const damien = new User() ;
damien.name = 'Carcy' ;
damien.firstName = 'Damien' ;
damien.id = 'dcarcy' ;
damien.password = 'admin';
listUsers = ajouter(damien, listUsers) ;

//Pseudo test ; expect listUsers got 2 items
console.log(`Items: ${listUsers.length}`) ; 

(user.id = 'dcarcy');

listUsers.forEach((user)=> {
    console.log(`User: ${user.name} : ${user.id}`)
}) ;

// exercice encrypt - decrypt



let pass = 'admin';

function encrypt (pass) {
let encryptPass = '';
for (let index = 0; index < pass.length; index++) {
    encryptPass += pass.charCodeAt(index).toString(16); 
    }
    return encryptPass ;
}





console.log(encryptPass);



let decryptPass = String.fromCharCode(encryptPass);


console.log(decryptPass);

/**
 * CONST = On ne peux pas ajouter/supprimer attributs mais on peut modifier une valeur
 * Attention il est possible de modifier la valeur id après ajout (user.id = dcarcy ;)
 * ForEach : libre d'utiliser n'importe terme comme élement
 * JS -> trés permissif car langage orienté prototype
 */