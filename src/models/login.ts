import { User } from "./user";
import { UserCollection } from "../collection/user-collection";
import { Collection } from "../collection/collection";

export class Login {
    public userId!: string;
    public userPass!: string;
    public repository!: UserCollection;

    public constructor(repository: UserCollection) {
        this.repository = repository;
    }

    public setCredential(userId: string, userPass: string) {
        this.userId = userId;
        this.userPass = userPass;
    }

    public process(): boolean {
        const usersList: User[] = this.repository.all();
        let find:boolean = false;
        for (let index: number = 0; index < usersList.length; index++) {
            const user: User = usersList[index]
            if (user.getID() === this.userId && user.getPassword() === this.userPass) {
                find= true;
            } 
        } 
        return find;

    }


}