export class User {
    public lastName!: string;
    public firstName!: string;
    private id!: string;
    private password!: string;

    public setId(id: string): void {
        if (this.id === undefined) {
            this.id = id ;
        }
        
    }

    public getID(): string {
        return this.id ;
    }

    public setPassword(password: string): void {
        this.password = password ;
    }

    public getPassword(): string {
        return this.password ;
    }

    public toString(): string {
        return  `
        ${this.firstName} + ${this.lastName}
        ID: ${this.id}
        Password: Not availaible
        `;
    }

}