"use strict";
exports.__esModule = true;
exports.Collection = void 0;
var Collection = /** @class */ (function () {
    function Collection() {
        this.collection = [];
    }
    Collection.prototype.add = function (item) {
        this.collection.push(item);
    };
    Collection.prototype.update = function (item) { };
    Collection.prototype.remove = function (item) { };
    Collection.prototype.size = function () {
        return this.collection.length;
    };
    Collection.prototype.all = function () {
        return this.collection;
    };
    return Collection;
}());
exports.Collection = Collection;
