import { Collection } from "./collection";
import { User } from "../models/user";

export class UserCollection extends Collection<User> {

    /**
     * @override
     * @param user 
     */

    public add(user:User): void {
        let indexOfUserInCollection: number = -1;

        indexOfUserInCollection = this.collection.findIndex(
            (userInCollection: User) => userInCollection.getID() === user.getID()
        );

        if (indexOfUserInCollection === -1 ) {
            super.add(user);
        }
    }

}

/* Methode alt if (this.collection.findIndex( (item: User) => item.getID()=== item.getID() ) === -1) {
            super.add(user);*/