let myName = ' ';

// Affecter la variable myName
myName = 'Carcy';

// Affichage du contenu
console.log(myName); // Expected: Carcy

// Changement valeur myName
myName = 'Damien';

// Affichage du contenu
console.log(myName); // Expected: Carcy Damien

let isMale = true;

let users = [
    'Damien',
    'Abdel',
    'Necibe',
    'Rainui',
    'Remy'
];

//Affecter yourName
let yourName = 'Test';
console.log(yourName);

myName = yourName;
yourName = 'a';

console.log(`${myName} <=> ${yourName}`);

//inconditionnelle
for (let index = 0; index <= 4; index = index + 1) {
    console.log(users[index]);
}

//conditionnelle
let indice = 0;
while (indice < users.length) {
    console.log(users[indice] );
    indice = indice + 1 ;
}