let userAges = [
    [
        'Jean-Luc', 53
    ],
    [
        'Bond', 35
    ],
    [
        'Joker', 75
    ]
];

// 1. Afficher la liste des utilisateurs avec leur age
// Jean-Luc 53
// Bond 35
// Joker 75

// 2. Dites bonjour aux utilisateurs de moins de 50 ans 
// Bonjour Bond

// 3. Calculer l'age moyen des utilisateurs
// Attendu 54.333

// Uniquement if , for, while

//1
let indice = 0 ;
while (indice < userAges.length){
    let user = userAges[indice];
    console.log(`${user[0]} ${user[1]}`);
    indice = indice + 1 ;
}

//2
let index = 0;
for (let index = 0; index < userAges.length; index = index + 1) {
    let user = userAges[index];
    if (user [1] <= 50) {
        console.log(`Bonjour ${user[0]}`);
    }  
}
//3
let ageMoyen = 0;
let total = 0;

for (let index = 0; index <userAges.length; index = index + 1) {
    let user = userAges[index];
    total = total + user[1];
    ageMoyen = total/(userAges.length);
}

console.log(ageMoyen)

//alt avec foreach
let somme = 0;
let moyen = 0

userAges.forEach(element => {
    let newUser = element;
    somme = somme + newUser[1];
})
moyen = somme/(userAges.length);
console.log(moyen); //54.33333 expected


    




